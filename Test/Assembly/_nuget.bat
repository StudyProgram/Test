echo off


cd /d %~dp0
set /p USR_INPUT_STR1=欲しいパッケージ名を一部でも良いので入力して下さい。関連するパッケージをリストアップします。

nuget list %USR_INPUT_STR1%


set /p USR_INPUT_STR2=インストールするパッケージを入力して下さい。

nuget install %USR_INPUT_STR2%

pause
exit
