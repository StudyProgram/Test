﻿/*
 * Created by SharpDevelop.
 * User: kouzimiso
 * Date: 2017/09/22
 * Time: 14:56
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
//using System;
//using System.Collections.Generic;
//using System.Text;
//using System.Windows;
//using System.Windows.Controls;
//using System.Windows.Data;
//using System.Windows.Documents;
//using System.Windows.Input;
//using System.Windows.Media;
//3Dが使えるようにする
//using System.Windows.Media.Media3D;//追加
//タイマーが使えるようにする
//using System.Windows.Threading;//追加


namespace XamlGuiControl
{
    /// <summary>
    /// Interaction logic for Page1.xaml
    /// </summary>
    public partial class Page1 : System.Windows.Controls.Page
    {

        private System.Windows.Threading.DispatcherTimer intervalTimer = new System.Windows.Threading.DispatcherTimer();
        //キャラの位置
        System.Windows.Media.Media3D.Vector3D pos = new System.Windows.Media.Media3D.Vector3D(0, 0, 0);
        //キャラの向き
        float angle = 0;

        public Page1()
        {
            this.InitializeComponent();
            //30ミリ秒ごとに、『Update3DEvent』関数を呼び出す
            intervalTimer.Interval = System.TimeSpan.FromMilliseconds(30);
            intervalTimer.Tick += new System.EventHandler(Update3DEvent);
            intervalTimer.Start();
        }
        //タイマー

        //30ミリ秒ごとに呼ばれる関数
        void Update3DEvent(object sender, System.EventArgs e)
        {
            if (System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.Right))
            {
                angle = 90;
                pos.X += 10;
            }
            if (System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.Left))
            {
                angle = 270;
                pos.X -= 10;
            }
            if (System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.Up))
            {
                angle = 180;
                pos.Z -= 10;
            }
            if (System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.Down))
            {
                angle = 0;
                pos.Z += 10;
            }
            //3D行列
            System.Windows.Media.Media3D.Matrix3D m = new System.Windows.Media.Media3D.Matrix3D();
            //回転
            System.Windows.Media.Media3D.Quaternion q = new System.Windows.Media.Media3D.Quaternion(new System.Windows.Media.Media3D.Vector3D(0, 1, 0), angle);
            //回転を3D行列にセット
            m.Rotate(q);
            //3D行列に平行移動した位置posをセット
            m.OffsetX = pos.X;
            m.OffsetY = pos.Y;
            m.OffsetZ = pos.Z;
            //3D行列を3D変形行列にセット
            System.Windows.Media.Media3D.MatrixTransform3D trans = new System.Windows.Media.Media3D.MatrixTransform3D(m);
            //剣士キャラに移動した変形行列をセット
            Character_3Dtest_Hero.Transform = trans;
        }



    }
}