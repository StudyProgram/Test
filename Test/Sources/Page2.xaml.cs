﻿/*
 * Created by SharpDevelop.
 * User: a2003408
 * Date: 2017/09/11
 * Time: 12:01
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

//using Microsoft.BCL.Async;    //ngetにて Assemblyのみ必要
//using Microsoft.Threading.Tasks;    //ngetにて Assemblyのみ必要
//using Microsoft.Threading.Tasks.Extensions;   //ngetにて Assemblyのみ必要
namespace XamlGuiControl
{
	/// <summary>
	/// Interaction logic for Page2.xaml
	/// </summary>
	public  partial class Page2 : System.Windows.Controls.Page
	{
		public Page2()
		{
			
			this.InitializeComponent();
            this.DataGrid_Data.DataContext = _data;
            //this.DataGrid_Data.DataContext = new string[]{"1","A1","A2"};
            //this.DataGrid_Data.DataContext = new string[,]{{"1","A1","A2"},{"1","A1","A3"},{"1","A1","A2"},{"1","A1","A2"}	};
            //System.Data.DataTable table = new System.Data.DataTable();
            //string[,] RowData=new string[,]{{"1","A1","A2"},{"1","A1","A3"},{"1","A1","A2"},{"1","A1","A2"}};
            //var RowData=new string[,]{{"1","A1","A2"},{"1","A1","A3"},{"1","A1","A2"},{"1","A1","A2"}};
            //table.Rows.Add(RowData);
            //this.DataGrid_Data.DataContext=table;
        }
        public int ConsoleProcessing;//{get ;set ;}
		// 表示するデータ
		private DataCollection _data= new DataCollection();

 

        //ファイル管理(テキスト)

        private async void FileOpenButton_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			Microsoft.Win32.OpenFileDialog  dialog = new  Microsoft.Win32.OpenFileDialog();
			dialog.Title = "ファイルを開く";
			dialog.Filter = "全てのファイル(*.*)|*.*";
			if (dialog.ShowDialog() == true)
			{
				string content=System.IO.File.ReadAllText(dialog.FileName);
				textbox1.Text=content;
				System.Data.DataTable dt = GetCsvDataTable(content);
				this.DataGrid_Data.ItemsSource = dt.DefaultView;
			}
		}

        private async void FileSaveAsButton_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			Microsoft.Win32.SaveFileDialog  dialog = new  Microsoft.Win32.SaveFileDialog();
			dialog.Title = "ファイルを保存";
			dialog.Filter = "テキストファイル|*.txt";
			if (dialog.ShowDialog() == true)
			{
				System.IO.File.WriteAllText( dialog.FileName,textbox1.Text);
			}

		}



        //ファイル管理(CSV)


        private void OpenCommandHandler(object sender, System.Windows.Input.ExecutedRoutedEventArgs e)
		 {
			Microsoft.Win32.OpenFileDialog Dialog = new Microsoft.Win32.OpenFileDialog();
			if (Dialog.ShowDialog() == true)
			{
				string content = System.IO.File.ReadAllText(Dialog.FileName, System.Text.Encoding.GetEncoding("Shift_JIS"));
				System.Data.DataTable dt = GetCsvDataTable(content);
				this.DataGrid_Data.ItemsSource = dt.DefaultView;
			}
		 }

		 private System.Data.DataTable GetCsvDataTable(string content)
		 {
			System.Collections.Generic.List<System.Collections.Generic.List<string>> lists = new System.Collections.Generic.List<System.Collections.Generic.List<string>>();
			System.Collections.Generic.List<string> lastList = new System.Collections.Generic.List<string>();
			lists.Add(lastList); //1行目を追加
			lastList.Add("");
			System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(",|\\r?\\n|[^,\"\\r\\n][^,\\r\\n]*|\"(?:[^\"]|\"\")*\"");
			System.Text.RegularExpressions.MatchCollection mc = regex.Matches(System.Text.RegularExpressions.Regex.Replace(content, "\\r?\\n$", ""));
			foreach (System.Text.RegularExpressions.Match m in mc)
			{
				switch (m.Value)
				{
				 case ",":
					lastList.Add("");
					break;
				 case "\n":
				 case "\r\n":
					//改行コードの場合は行を追加する
					lastList = new System.Collections.Generic.List<string>();
					lists.Add(lastList);
					lastList.Add("");
					break;
				 default:
					if (m.Value.StartsWith("\""))
					{
					 //ダブルクォートで囲われている場合は最初と最後のダブルクォートを外し、
					 //文字列中のダブルクォートのエスケープをアンエスケープする
					 lastList[lastList.Count - 1] =
						 m.Value.Substring(1, m.Value.Length - 2).Replace("\"\"", "\"");
					}
					else
					{
					 lastList[lastList.Count - 1] = m.Value;
					}
					break;
			 }
			}

			// データテーブルにコピーする
			System.Data.DataTable dt = new System.Data.DataTable();
			for (int i = 0; i < lists[0].Count; i++)
			{
			 dt.Columns.Add();
			}
			foreach (System.Collections.Generic.List<string> list in lists)
			{
			 System.Data.DataRow dr = dt.NewRow();
			 for (int i = 0; i < list.Count; i++)
			 {
				 dr[i] = list[i];
			 }
			 dt.Rows.Add(dr);
			}
			return dt;
		}


    }



    // 表示する個々のデータ（データバインド可能）

    public class LineData : System.ComponentModel.INotifyPropertyChanged
	{

		// Indexプロパティ

		string _index;
		public string Index
		{
			get{ return _index; }
			set{_index = value; OnPropertyChanged("Index");}
		}

		// Valueプロパティ

		string _value;
		public string Value
		{
			get { return _value; }
			set { _value = value; OnPropertyChanged("Value"); }
		}

		string _name;
		public string Name
		{
			get { return _name; }
			set { _name = value; OnPropertyChanged("name"); }
		}



		// INotifyPropertyChangedインターフェースの実装

		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
		void OnPropertyChanged(string pName)
		{
			var handler = this.PropertyChanged;
			if (handler != null)
				handler(this, new System.ComponentModel.PropertyChangedEventArgs(pName));
		}
	}



	// 表示するデータのコレクション（データバインド可能）

	public class DataCollection : System.Collections.ObjectModel.ObservableCollection<LineData>
	{
		public DataCollection()
		{
			// 初期データ
			this.Add(new LineData()
			{
				Index="1", Value="Red",Name="test"
			});

			this.Add(new LineData()
			{
				Index="2", Value="Green"
			});

			this.Add(new LineData()
			{
				Index="3", Value="Blue"
			});
		}
	}
}	